import os

from cx_Freeze import Executable, setup


executables = [
    Executable(
        script='main.py',
        targetName='Renamer.exe',
        base='Win32GUI',
        icon='icon.ico',
    )
]
zip_include_packages = [
    'os',
    'yaml',
    'tkinter',
]
exclude_packages = [
    'asyncio',
    'distutils',
    'concurrent',
    'email',
    'html',
    'http',
    'json',
    'unittest',
    'pydoc_data',
    'xml',
    'urllib',
]
build_exe_path = os.path.join(os.getcwd(), 'build\\exe.win-amd64-3.8')


setup(
    name='Renamer',
    version='1.0.0',
    options={
        'build_exe': {
            'zip_include_packages': zip_include_packages,
            'include_msvcr': True,
            'build_exe': build_exe_path,
            'excludes': exclude_packages,
        },
        'bdist_msi': {
            'initial_target_dir': 'C:\\ProgramData\\Renamer',
        }
    },
    executables=executables,
)
