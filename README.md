# FileRenamer

Rename files in directory containing a certain substring in name

## Create application installer for Windows
python setup.py bdist_msi

## Linters
- `flake8`
- `isort .` (to check only - `isort --check-only --diff .`)
- `pylint --rcfile pylintrc src`
