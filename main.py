import logging

from src.app.window_configurator import Application


logger = logging.getLogger(__name__)


def main():
    logger.info('Application is launching')
    application = Application()
    application.run()


if __name__ == '__main__':
    main()
