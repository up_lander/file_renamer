import os
from unittest.mock import call, patch

import pytest

from src.app.file_processor import rename_paths


@patch('src.app.file_processor.os')
class TestFileProcessor:
    directory = os.path.join('C', 'dir')

    def test_failed_rename_paths(self, mocked_os):
        """Try to rename file to the name which already exists in directory"""
        paths = [
            os.path.join(self.directory, 'file.txt'),  # change file to test leads to name conflict
            os.path.join(self.directory, 'test.txt'),
        ]
        old = 'file'
        new = 'test'
        mocked_os.path = os.path  # hacky for correct unmocked path
        with pytest.raises(FileExistsError):
            rename_paths(paths, old, new)
        assert mocked_os.rename.call_count == 0, 'os.rename shouldn\'t use if there is a problem in resulted names'

    def test_succeeded_rename_paths(self, mocked_os):
        """Try to rename file to the name which already exists in directory"""
        paths = [
            os.path.join(self.directory, 'file.txt'),
            os.path.join(self.directory, 'another_file.txt'),
        ]
        old = 'file'
        new = 'test'
        mocked_os.path = os.path  # hacky for correct unmocked path
        rename_paths(paths, old, new)
        calls = [
            call(os.path.join(self.directory, 'file.txt'), os.path.join(self.directory, 'test.txt')),
            call(os.path.join(self.directory, 'another_file.txt'), os.path.join(self.directory, 'another_test.txt')),
        ]
        mocked_os.rename.assert_has_calls(calls)
