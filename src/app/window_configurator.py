import logging.config
import tkinter as tk
from tkinter import filedialog, messagebox

from src import settings
from src.app.file_processor import rename_paths


logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger(__name__)


class Application:
    def __init__(self):
        self.master = None

        self.path_entry = None
        self.old_string_entry = None
        self.new_string_entry = None
        self.files = None

    def _get_pads(self):
        padx = self.master.winfo_screenwidth()
        pady = self.master.winfo_screenheight()
        padx = padx // 2
        pady = pady // 2
        return padx - settings.WINDOW_WIDTH // 2, pady - settings.WINDOW_HEIGHT // 2

    def prepare_master(self):
        self.master = tk.Tk()
        self.master.title(f"{settings.WINDOW_TITLE}")
        padx, pady = self._get_pads()
        logger.info(f"geometry is {settings.WINDOW_WIDTH}x{settings.WINDOW_HEIGHT}+{padx}+{pady}")
        self.master.geometry(f"{settings.WINDOW_WIDTH}x{settings.WINDOW_HEIGHT}+{padx}+{pady}")
        self.master.resizable(False, False)

    def _add_empty_row(self, row):
        tk.Label(
            self.master,
            text=settings.EMPTY_ROW_LABEL_TEXT,
            width=settings.LABEL_WIDTH,
            font=settings.FONT_CONFIG,
        ).grid(row=row)

    def prepare_labels(self):
        self._add_empty_row(row=0)
        self._add_empty_row(row=2)
        tk.Label(
            self.master,
            text=settings.LABEL_OLD_SUBSTRING,
            width=settings.LABEL_WIDTH,
            font=settings.FONT_CONFIG,
        ).grid(row=3)
        self._add_empty_row(row=4)
        tk.Label(
            self.master,
            text=settings.LABEL_NEW_SUBSTRING,
            width=settings.LABEL_WIDTH,
            font=settings.FONT_CONFIG,
        ).grid(row=5)
        self._add_empty_row(row=6)

    def prepare_entries(self):
        self.path_entry = tk.Entry(self.master, width=settings.ENTRY_WIDTH, font=settings.FONT_CONFIG)
        self.old_string_entry = tk.Entry(self.master, width=settings.ENTRY_WIDTH, font=settings.FONT_CONFIG)
        self.new_string_entry = tk.Entry(self.master, width=settings.ENTRY_WIDTH, font=settings.FONT_CONFIG)

        self.path_entry.grid(row=1, column=1)
        self.path_entry.insert(0, settings.PATH_ENTRY_DEFAULT)
        self.old_string_entry.grid(row=3, column=1)
        self.new_string_entry.grid(row=5, column=1)

    def get_files(self):
        initial_dir = self.path_entry.get()
        self.files = filedialog.askopenfilenames(initialdir=initial_dir)

    def prepare_buttons(self):
        tk.Button(
            self.master,
            text=settings.BROWSE_BUTTON_TEXT,
            command=self.get_files,
            font=settings.FONT_CONFIG,
        ).grid(row=1, column=0)

        tk.Button(
            self.master,
            text=settings.RENAME_BUTTON_TEXT,
            command=self.rename,
            font=settings.FONT_CONFIG,
        ).grid(row=7, column=1)

    def rename(self):
        def wrapper(old, new):
            if not self.files or not old or not new:
                raise RuntimeError("data is empty")
            logger.info('get paths is running')
            rename_paths(self.files, old, new)
            logger.info('renaming is finished')
            messagebox.showinfo(settings.SUCCESS_TITLE, settings.SUCCESS_MESSAGE)

        old = self.old_string_entry.get()
        new = self.new_string_entry.get()
        logger.info(f'filenames are {self.files}, old substring - {old}, new substring - {new}')
        try:
            return wrapper(old, new)
        except FileExistsError as e:
            logger.error(e)
            messagebox.showerror(settings.ERROR_TITLE, settings.DUPLICATE_FILENAMES_ERROR_MESSAGE)
        except RuntimeError as e:
            logger.error(e)
            messagebox.showerror(settings.ERROR_TITLE, settings.EMPTY_DATA_ERROR)
        except Exception as e:
            logger.error(e)
            messagebox.showerror(settings.ERROR_TITLE, settings.UNEXPECTED_ERROR_MESSAGE)
        return

    def run(self):
        logger.info('prepare master')
        self.prepare_master()
        logger.info('prepare labels')
        self.prepare_labels()
        logger.info('prepare entries')
        self.prepare_entries()
        logger.info('prepare buttons')
        self.prepare_buttons()
        logger.info('run mainloop')
        self.master.mainloop()
