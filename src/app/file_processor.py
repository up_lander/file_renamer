import logging
import os
from collections import Counter


def _prepare_mapper(paths, old, new):
    path_mapper = {}
    for old_path in paths:
        directory = os.path.dirname(old_path)
        old_name = os.path.basename(old_path)
        new_name = old_name.replace(old, new)
        new_path = os.path.join(directory, new_name)
        path_mapper[old_path] = new_path
    return path_mapper


def _exist_identical_paths(paths):
    words_amount = Counter(paths).values()
    return max(words_amount) > 1


def rename_paths(paths, old, new):
    logger = logging.getLogger(f'{__name__}.rename_paths')
    path_mapper = _prepare_mapper(paths, old, new)

    # raise FileExistsError before renaming process
    if _exist_identical_paths(path_mapper.values()):
        raise FileExistsError("there are too many files with the same name")

    for old_path, new_path in path_mapper.items():
        logger.info(f"old path - {old_path}, new - {new_path}")
        os.rename(old_path, new_path)
